import Vue from 'vue';
import { mapActions, mapState } from 'vuex';
import App from './App.vue';
import store from './store';

Vue.config.productionTip = false;

// inject a handler for `myOption` custom option
Vue.mixin({
  computed: {
    ...mapState([
      'products',
      'filteredProducts',
      'selectedProducts',
    ]),
  },
  methods: {
    ...mapActions([
      'removeItem',
      'addItem',
    ]),
  },
});

new Vue({
  store,
  render: h => h(App),
  // created() {
  //   let x = 0;
  //   const productsList = [];
  //   while (x < 300) {
  //     productsList.push({
  //       id: x,
  //       name: `Product ${x}`,
  //       ref: `SHP${(100000 + x)}`,
  //     });
  //     x += 1;
  //   }
  //   console.log(productsList);
  // },
}).$mount('#app');
