import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    filteredProducts: [],
    products: [
      {
        id: 0,
        name: 'Product 0',
        ref: 'SHP100000',
      },
      {
        id: 1,
        name: 'Product 1',
        ref: 'SHP100001',
      },
      {
        id: 2,
        name: 'Product 2',
        ref: 'SHP100002',
      },
      {
        id: 3,
        name: 'Product 3',
        ref: 'SHP100003',
      },
      {
        id: 4,
        name: 'Product 4',
        ref: 'SHP100004',
      },
      {
        id: 5,
        name: 'Product 5',
        ref: 'SHP100005',
      },
      {
        id: 6,
        name: 'Product 6',
        ref: 'SHP100006',
      },
      {
        id: 7,
        name: 'Product 7',
        ref: 'SHP100007',
      },
      {
        id: 8,
        name: 'Product 8',
        ref: 'SHP100008',
      },
      {
        id: 9,
        name: 'Product 9',
        ref: 'SHP100009',
      },
      {
        id: 10,
        name: 'Product 10',
        ref: 'SHP100010',
      },
      {
        id: 11,
        name: 'Product 11',
        ref: 'SHP100011',
      },
      {
        id: 12,
        name: 'Product 12',
        ref: 'SHP100012',
      },
      {
        id: 13,
        name: 'Product 13',
        ref: 'SHP100013',
      },
      {
        id: 14,
        name: 'Product 14',
        ref: 'SHP100014',
      },
      {
        id: 15,
        name: 'Product 15',
        ref: 'SHP100015',
      },
      {
        id: 16,
        name: 'Product 16',
        ref: 'SHP100016',
      },
      {
        id: 17,
        name: 'Product 17',
        ref: 'SHP100017',
      },
      {
        id: 18,
        name: 'Product 18',
        ref: 'SHP100018',
      },
      {
        id: 19,
        name: 'Product 19',
        ref: 'SHP100019',
      },
      {
        id: 20,
        name: 'Product 20',
        ref: 'SHP100020',
      },
      {
        id: 21,
        name: 'Product 21',
        ref: 'SHP100021',
      },
      {
        id: 22,
        name: 'Product 22',
        ref: 'SHP100022',
      },
      {
        id: 23,
        name: 'Product 23',
        ref: 'SHP100023',
      },
      {
        id: 24,
        name: 'Product 24',
        ref: 'SHP100024',
      },
      {
        id: 25,
        name: 'Product 25',
        ref: 'SHP100025',
      },
      {
        id: 26,
        name: 'Product 26',
        ref: 'SHP100026',
      },
      {
        id: 27,
        name: 'Product 27',
        ref: 'SHP100027',
      },
      {
        id: 28,
        name: 'Product 28',
        ref: 'SHP100028',
      },
      {
        id: 29,
        name: 'Product 29',
        ref: 'SHP100029',
      },
      {
        id: 30,
        name: 'Product 30',
        ref: 'SHP100030',
      },
      {
        id: 31,
        name: 'Product 31',
        ref: 'SHP100031',
      },
      {
        id: 32,
        name: 'Product 32',
        ref: 'SHP100032',
      },
      {
        id: 33,
        name: 'Product 33',
        ref: 'SHP100033',
      },
      {
        id: 34,
        name: 'Product 34',
        ref: 'SHP100034',
      },
      {
        id: 35,
        name: 'Product 35',
        ref: 'SHP100035',
      },
      {
        id: 36,
        name: 'Product 36',
        ref: 'SHP100036',
      },
      {
        id: 37,
        name: 'Product 37',
        ref: 'SHP100037',
      },
      {
        id: 38,
        name: 'Product 38',
        ref: 'SHP100038',
      },
      {
        id: 39,
        name: 'Product 39',
        ref: 'SHP100039',
      },
      {
        id: 40,
        name: 'Product 40',
        ref: 'SHP100040',
      },
      {
        id: 41,
        name: 'Product 41',
        ref: 'SHP100041',
      },
      {
        id: 42,
        name: 'Product 42',
        ref: 'SHP100042',
      },
      {
        id: 43,
        name: 'Product 43',
        ref: 'SHP100043',
      },
      {
        id: 44,
        name: 'Product 44',
        ref: 'SHP100044',
      },
      {
        id: 45,
        name: 'Product 45',
        ref: 'SHP100045',
      },
      {
        id: 46,
        name: 'Product 46',
        ref: 'SHP100046',
      },
      {
        id: 47,
        name: 'Product 47',
        ref: 'SHP100047',
      },
      {
        id: 48,
        name: 'Product 48',
        ref: 'SHP100048',
      },
      {
        id: 49,
        name: 'Product 49',
        ref: 'SHP100049',
      },
      {
        id: 50,
        name: 'Product 50',
        ref: 'SHP100050',
      },
      {
        id: 51,
        name: 'Product 51',
        ref: 'SHP100051',
      },
      {
        id: 52,
        name: 'Product 52',
        ref: 'SHP100052',
      },
      {
        id: 53,
        name: 'Product 53',
        ref: 'SHP100053',
      },
      {
        id: 54,
        name: 'Product 54',
        ref: 'SHP100054',
      },
      {
        id: 55,
        name: 'Product 55',
        ref: 'SHP100055',
      },
      {
        id: 56,
        name: 'Product 56',
        ref: 'SHP100056',
      },
      {
        id: 57,
        name: 'Product 57',
        ref: 'SHP100057',
      },
      {
        id: 58,
        name: 'Product 58',
        ref: 'SHP100058',
      },
      {
        id: 59,
        name: 'Product 59',
        ref: 'SHP100059',
      },
      {
        id: 60,
        name: 'Product 60',
        ref: 'SHP100060',
      },
      {
        id: 61,
        name: 'Product 61',
        ref: 'SHP100061',
      },
      {
        id: 62,
        name: 'Product 62',
        ref: 'SHP100062',
      },
      {
        id: 63,
        name: 'Product 63',
        ref: 'SHP100063',
      },
      {
        id: 64,
        name: 'Product 64',
        ref: 'SHP100064',
      },
      {
        id: 65,
        name: 'Product 65',
        ref: 'SHP100065',
      },
      {
        id: 66,
        name: 'Product 66',
        ref: 'SHP100066',
      },
      {
        id: 67,
        name: 'Product 67',
        ref: 'SHP100067',
      },
      {
        id: 68,
        name: 'Product 68',
        ref: 'SHP100068',
      },
      {
        id: 69,
        name: 'Product 69',
        ref: 'SHP100069',
      },
      {
        id: 70,
        name: 'Product 70',
        ref: 'SHP100070',
      },
      {
        id: 71,
        name: 'Product 71',
        ref: 'SHP100071',
      },
      {
        id: 72,
        name: 'Product 72',
        ref: 'SHP100072',
      },
      {
        id: 73,
        name: 'Product 73',
        ref: 'SHP100073',
      },
      {
        id: 74,
        name: 'Product 74',
        ref: 'SHP100074',
      },
      {
        id: 75,
        name: 'Product 75',
        ref: 'SHP100075',
      },
      {
        id: 76,
        name: 'Product 76',
        ref: 'SHP100076',
      },
      {
        id: 77,
        name: 'Product 77',
        ref: 'SHP100077',
      },
      {
        id: 78,
        name: 'Product 78',
        ref: 'SHP100078',
      },
      {
        id: 79,
        name: 'Product 79',
        ref: 'SHP100079',
      },
      {
        id: 80,
        name: 'Product 80',
        ref: 'SHP100080',
      },
      {
        id: 81,
        name: 'Product 81',
        ref: 'SHP100081',
      },
      {
        id: 82,
        name: 'Product 82',
        ref: 'SHP100082',
      },
      {
        id: 83,
        name: 'Product 83',
        ref: 'SHP100083',
      },
      {
        id: 84,
        name: 'Product 84',
        ref: 'SHP100084',
      },
      {
        id: 85,
        name: 'Product 85',
        ref: 'SHP100085',
      },
      {
        id: 86,
        name: 'Product 86',
        ref: 'SHP100086',
      },
      {
        id: 87,
        name: 'Product 87',
        ref: 'SHP100087',
      },
      {
        id: 88,
        name: 'Product 88',
        ref: 'SHP100088',
      },
      {
        id: 89,
        name: 'Product 89',
        ref: 'SHP100089',
      },
      {
        id: 90,
        name: 'Product 90',
        ref: 'SHP100090',
      },
      {
        id: 91,
        name: 'Product 91',
        ref: 'SHP100091',
      },
      {
        id: 92,
        name: 'Product 92',
        ref: 'SHP100092',
      },
      {
        id: 93,
        name: 'Product 93',
        ref: 'SHP100093',
      },
      {
        id: 94,
        name: 'Product 94',
        ref: 'SHP100094',
      },
      {
        id: 95,
        name: 'Product 95',
        ref: 'SHP100095',
      },
      {
        id: 96,
        name: 'Product 96',
        ref: 'SHP100096',
      },
      {
        id: 97,
        name: 'Product 97',
        ref: 'SHP100097',
      },
      {
        id: 98,
        name: 'Product 98',
        ref: 'SHP100098',
      },
      {
        id: 99,
        name: 'Product 99',
        ref: 'SHP100099',
      },
      {
        id: 100,
        name: 'Product 100',
        ref: 'SHP100100',
      },
      {
        id: 101,
        name: 'Product 101',
        ref: 'SHP100101',
      },
      {
        id: 102,
        name: 'Product 102',
        ref: 'SHP100102',
      },
      {
        id: 103,
        name: 'Product 103',
        ref: 'SHP100103',
      },
      {
        id: 104,
        name: 'Product 104',
        ref: 'SHP100104',
      },
      {
        id: 105,
        name: 'Product 105',
        ref: 'SHP100105',
      },
      {
        id: 106,
        name: 'Product 106',
        ref: 'SHP100106',
      },
      {
        id: 107,
        name: 'Product 107',
        ref: 'SHP100107',
      },
      {
        id: 108,
        name: 'Product 108',
        ref: 'SHP100108',
      },
      {
        id: 109,
        name: 'Product 109',
        ref: 'SHP100109',
      },
      {
        id: 110,
        name: 'Product 110',
        ref: 'SHP100110',
      },
      {
        id: 111,
        name: 'Product 111',
        ref: 'SHP100111',
      },
      {
        id: 112,
        name: 'Product 112',
        ref: 'SHP100112',
      },
      {
        id: 113,
        name: 'Product 113',
        ref: 'SHP100113',
      },
      {
        id: 114,
        name: 'Product 114',
        ref: 'SHP100114',
      },
      {
        id: 115,
        name: 'Product 115',
        ref: 'SHP100115',
      },
      {
        id: 116,
        name: 'Product 116',
        ref: 'SHP100116',
      },
      {
        id: 117,
        name: 'Product 117',
        ref: 'SHP100117',
      },
      {
        id: 118,
        name: 'Product 118',
        ref: 'SHP100118',
      },
      {
        id: 119,
        name: 'Product 119',
        ref: 'SHP100119',
      },
      {
        id: 120,
        name: 'Product 120',
        ref: 'SHP100120',
      },
      {
        id: 121,
        name: 'Product 121',
        ref: 'SHP100121',
      },
      {
        id: 122,
        name: 'Product 122',
        ref: 'SHP100122',
      },
      {
        id: 123,
        name: 'Product 123',
        ref: 'SHP100123',
      },
      {
        id: 124,
        name: 'Product 124',
        ref: 'SHP100124',
      },
      {
        id: 125,
        name: 'Product 125',
        ref: 'SHP100125',
      },
      {
        id: 126,
        name: 'Product 126',
        ref: 'SHP100126',
      },
      {
        id: 127,
        name: 'Product 127',
        ref: 'SHP100127',
      },
      {
        id: 128,
        name: 'Product 128',
        ref: 'SHP100128',
      },
      {
        id: 129,
        name: 'Product 129',
        ref: 'SHP100129',
      },
      {
        id: 130,
        name: 'Product 130',
        ref: 'SHP100130',
      },
      {
        id: 131,
        name: 'Product 131',
        ref: 'SHP100131',
      },
      {
        id: 132,
        name: 'Product 132',
        ref: 'SHP100132',
      },
      {
        id: 133,
        name: 'Product 133',
        ref: 'SHP100133',
      },
      {
        id: 134,
        name: 'Product 134',
        ref: 'SHP100134',
      },
      {
        id: 135,
        name: 'Product 135',
        ref: 'SHP100135',
      },
      {
        id: 136,
        name: 'Product 136',
        ref: 'SHP100136',
      },
      {
        id: 137,
        name: 'Product 137',
        ref: 'SHP100137',
      },
      {
        id: 138,
        name: 'Product 138',
        ref: 'SHP100138',
      },
      {
        id: 139,
        name: 'Product 139',
        ref: 'SHP100139',
      },
      {
        id: 140,
        name: 'Product 140',
        ref: 'SHP100140',
      },
      {
        id: 141,
        name: 'Product 141',
        ref: 'SHP100141',
      },
      {
        id: 142,
        name: 'Product 142',
        ref: 'SHP100142',
      },
      {
        id: 143,
        name: 'Product 143',
        ref: 'SHP100143',
      },
      {
        id: 144,
        name: 'Product 144',
        ref: 'SHP100144',
      },
      {
        id: 145,
        name: 'Product 145',
        ref: 'SHP100145',
      },
      {
        id: 146,
        name: 'Product 146',
        ref: 'SHP100146',
      },
      {
        id: 147,
        name: 'Product 147',
        ref: 'SHP100147',
      },
      {
        id: 148,
        name: 'Product 148',
        ref: 'SHP100148',
      },
      {
        id: 149,
        name: 'Product 149',
        ref: 'SHP100149',
      },
      {
        id: 150,
        name: 'Product 150',
        ref: 'SHP100150',
      },
      {
        id: 151,
        name: 'Product 151',
        ref: 'SHP100151',
      },
      {
        id: 152,
        name: 'Product 152',
        ref: 'SHP100152',
      },
      {
        id: 153,
        name: 'Product 153',
        ref: 'SHP100153',
      },
      {
        id: 154,
        name: 'Product 154',
        ref: 'SHP100154',
      },
      {
        id: 155,
        name: 'Product 155',
        ref: 'SHP100155',
      },
      {
        id: 156,
        name: 'Product 156',
        ref: 'SHP100156',
      },
      {
        id: 157,
        name: 'Product 157',
        ref: 'SHP100157',
      },
      {
        id: 158,
        name: 'Product 158',
        ref: 'SHP100158',
      },
      {
        id: 159,
        name: 'Product 159',
        ref: 'SHP100159',
      },
      {
        id: 160,
        name: 'Product 160',
        ref: 'SHP100160',
      },
      {
        id: 161,
        name: 'Product 161',
        ref: 'SHP100161',
      },
      {
        id: 162,
        name: 'Product 162',
        ref: 'SHP100162',
      },
      {
        id: 163,
        name: 'Product 163',
        ref: 'SHP100163',
      },
      {
        id: 164,
        name: 'Product 164',
        ref: 'SHP100164',
      },
      {
        id: 165,
        name: 'Product 165',
        ref: 'SHP100165',
      },
      {
        id: 166,
        name: 'Product 166',
        ref: 'SHP100166',
      },
      {
        id: 167,
        name: 'Product 167',
        ref: 'SHP100167',
      },
      {
        id: 168,
        name: 'Product 168',
        ref: 'SHP100168',
      },
      {
        id: 169,
        name: 'Product 169',
        ref: 'SHP100169',
      },
      {
        id: 170,
        name: 'Product 170',
        ref: 'SHP100170',
      },
      {
        id: 171,
        name: 'Product 171',
        ref: 'SHP100171',
      },
      {
        id: 172,
        name: 'Product 172',
        ref: 'SHP100172',
      },
      {
        id: 173,
        name: 'Product 173',
        ref: 'SHP100173',
      },
      {
        id: 174,
        name: 'Product 174',
        ref: 'SHP100174',
      },
      {
        id: 175,
        name: 'Product 175',
        ref: 'SHP100175',
      },
      {
        id: 176,
        name: 'Product 176',
        ref: 'SHP100176',
      },
      {
        id: 177,
        name: 'Product 177',
        ref: 'SHP100177',
      },
      {
        id: 178,
        name: 'Product 178',
        ref: 'SHP100178',
      },
      {
        id: 179,
        name: 'Product 179',
        ref: 'SHP100179',
      },
      {
        id: 180,
        name: 'Product 180',
        ref: 'SHP100180',
      },
      {
        id: 181,
        name: 'Product 181',
        ref: 'SHP100181',
      },
      {
        id: 182,
        name: 'Product 182',
        ref: 'SHP100182',
      },
      {
        id: 183,
        name: 'Product 183',
        ref: 'SHP100183',
      },
      {
        id: 184,
        name: 'Product 184',
        ref: 'SHP100184',
      },
      {
        id: 185,
        name: 'Product 185',
        ref: 'SHP100185',
      },
      {
        id: 186,
        name: 'Product 186',
        ref: 'SHP100186',
      },
      {
        id: 187,
        name: 'Product 187',
        ref: 'SHP100187',
      },
      {
        id: 188,
        name: 'Product 188',
        ref: 'SHP100188',
      },
      {
        id: 189,
        name: 'Product 189',
        ref: 'SHP100189',
      },
      {
        id: 190,
        name: 'Product 190',
        ref: 'SHP100190',
      },
      {
        id: 191,
        name: 'Product 191',
        ref: 'SHP100191',
      },
      {
        id: 192,
        name: 'Product 192',
        ref: 'SHP100192',
      },
      {
        id: 193,
        name: 'Product 193',
        ref: 'SHP100193',
      },
      {
        id: 194,
        name: 'Product 194',
        ref: 'SHP100194',
      },
      {
        id: 195,
        name: 'Product 195',
        ref: 'SHP100195',
      },
      {
        id: 196,
        name: 'Product 196',
        ref: 'SHP100196',
      },
      {
        id: 197,
        name: 'Product 197',
        ref: 'SHP100197',
      },
      {
        id: 198,
        name: 'Product 198',
        ref: 'SHP100198',
      },
      {
        id: 199,
        name: 'Product 199',
        ref: 'SHP100199',
      },
      {
        id: 200,
        name: 'Product 200',
        ref: 'SHP100200',
      },
      {
        id: 201,
        name: 'Product 201',
        ref: 'SHP100201',
      },
      {
        id: 202,
        name: 'Product 202',
        ref: 'SHP100202',
      },
      {
        id: 203,
        name: 'Product 203',
        ref: 'SHP100203',
      },
      {
        id: 204,
        name: 'Product 204',
        ref: 'SHP100204',
      },
      {
        id: 205,
        name: 'Product 205',
        ref: 'SHP100205',
      },
      {
        id: 206,
        name: 'Product 206',
        ref: 'SHP100206',
      },
      {
        id: 207,
        name: 'Product 207',
        ref: 'SHP100207',
      },
      {
        id: 208,
        name: 'Product 208',
        ref: 'SHP100208',
      },
      {
        id: 209,
        name: 'Product 209',
        ref: 'SHP100209',
      },
      {
        id: 210,
        name: 'Product 210',
        ref: 'SHP100210',
      },
      {
        id: 211,
        name: 'Product 211',
        ref: 'SHP100211',
      },
      {
        id: 212,
        name: 'Product 212',
        ref: 'SHP100212',
      },
      {
        id: 213,
        name: 'Product 213',
        ref: 'SHP100213',
      },
      {
        id: 214,
        name: 'Product 214',
        ref: 'SHP100214',
      },
      {
        id: 215,
        name: 'Product 215',
        ref: 'SHP100215',
      },
      {
        id: 216,
        name: 'Product 216',
        ref: 'SHP100216',
      },
      {
        id: 217,
        name: 'Product 217',
        ref: 'SHP100217',
      },
      {
        id: 218,
        name: 'Product 218',
        ref: 'SHP100218',
      },
      {
        id: 219,
        name: 'Product 219',
        ref: 'SHP100219',
      },
      {
        id: 220,
        name: 'Product 220',
        ref: 'SHP100220',
      },
      {
        id: 221,
        name: 'Product 221',
        ref: 'SHP100221',
      },
      {
        id: 222,
        name: 'Product 222',
        ref: 'SHP100222',
      },
      {
        id: 223,
        name: 'Product 223',
        ref: 'SHP100223',
      },
      {
        id: 224,
        name: 'Product 224',
        ref: 'SHP100224',
      },
      {
        id: 225,
        name: 'Product 225',
        ref: 'SHP100225',
      },
      {
        id: 226,
        name: 'Product 226',
        ref: 'SHP100226',
      },
      {
        id: 227,
        name: 'Product 227',
        ref: 'SHP100227',
      },
      {
        id: 228,
        name: 'Product 228',
        ref: 'SHP100228',
      },
      {
        id: 229,
        name: 'Product 229',
        ref: 'SHP100229',
      },
      {
        id: 230,
        name: 'Product 230',
        ref: 'SHP100230',
      },
      {
        id: 231,
        name: 'Product 231',
        ref: 'SHP100231',
      },
      {
        id: 232,
        name: 'Product 232',
        ref: 'SHP100232',
      },
      {
        id: 233,
        name: 'Product 233',
        ref: 'SHP100233',
      },
      {
        id: 234,
        name: 'Product 234',
        ref: 'SHP100234',
      },
      {
        id: 235,
        name: 'Product 235',
        ref: 'SHP100235',
      },
      {
        id: 236,
        name: 'Product 236',
        ref: 'SHP100236',
      },
      {
        id: 237,
        name: 'Product 237',
        ref: 'SHP100237',
      },
      {
        id: 238,
        name: 'Product 238',
        ref: 'SHP100238',
      },
      {
        id: 239,
        name: 'Product 239',
        ref: 'SHP100239',
      },
      {
        id: 240,
        name: 'Product 240',
        ref: 'SHP100240',
      },
      {
        id: 241,
        name: 'Product 241',
        ref: 'SHP100241',
      },
      {
        id: 242,
        name: 'Product 242',
        ref: 'SHP100242',
      },
      {
        id: 243,
        name: 'Product 243',
        ref: 'SHP100243',
      },
      {
        id: 244,
        name: 'Product 244',
        ref: 'SHP100244',
      },
      {
        id: 245,
        name: 'Product 245',
        ref: 'SHP100245',
      },
      {
        id: 246,
        name: 'Product 246',
        ref: 'SHP100246',
      },
      {
        id: 247,
        name: 'Product 247',
        ref: 'SHP100247',
      },
      {
        id: 248,
        name: 'Product 248',
        ref: 'SHP100248',
      },
      {
        id: 249,
        name: 'Product 249',
        ref: 'SHP100249',
      },
      {
        id: 250,
        name: 'Product 250',
        ref: 'SHP100250',
      },
      {
        id: 251,
        name: 'Product 251',
        ref: 'SHP100251',
      },
      {
        id: 252,
        name: 'Product 252',
        ref: 'SHP100252',
      },
      {
        id: 253,
        name: 'Product 253',
        ref: 'SHP100253',
      },
      {
        id: 254,
        name: 'Product 254',
        ref: 'SHP100254',
      },
      {
        id: 255,
        name: 'Product 255',
        ref: 'SHP100255',
      },
      {
        id: 256,
        name: 'Product 256',
        ref: 'SHP100256',
      },
      {
        id: 257,
        name: 'Product 257',
        ref: 'SHP100257',
      },
      {
        id: 258,
        name: 'Product 258',
        ref: 'SHP100258',
      },
      {
        id: 259,
        name: 'Product 259',
        ref: 'SHP100259',
      },
      {
        id: 260,
        name: 'Product 260',
        ref: 'SHP100260',
      },
      {
        id: 261,
        name: 'Product 261',
        ref: 'SHP100261',
      },
      {
        id: 262,
        name: 'Product 262',
        ref: 'SHP100262',
      },
      {
        id: 263,
        name: 'Product 263',
        ref: 'SHP100263',
      },
      {
        id: 264,
        name: 'Product 264',
        ref: 'SHP100264',
      },
      {
        id: 265,
        name: 'Product 265',
        ref: 'SHP100265',
      },
      {
        id: 266,
        name: 'Product 266',
        ref: 'SHP100266',
      },
      {
        id: 267,
        name: 'Product 267',
        ref: 'SHP100267',
      },
      {
        id: 268,
        name: 'Product 268',
        ref: 'SHP100268',
      },
      {
        id: 269,
        name: 'Product 269',
        ref: 'SHP100269',
      },
      {
        id: 270,
        name: 'Product 270',
        ref: 'SHP100270',
      },
      {
        id: 271,
        name: 'Product 271',
        ref: 'SHP100271',
      },
      {
        id: 272,
        name: 'Product 272',
        ref: 'SHP100272',
      },
      {
        id: 273,
        name: 'Product 273',
        ref: 'SHP100273',
      },
      {
        id: 274,
        name: 'Product 274',
        ref: 'SHP100274',
      },
      {
        id: 275,
        name: 'Product 275',
        ref: 'SHP100275',
      },
      {
        id: 276,
        name: 'Product 276',
        ref: 'SHP100276',
      },
      {
        id: 277,
        name: 'Product 277',
        ref: 'SHP100277',
      },
      {
        id: 278,
        name: 'Product 278',
        ref: 'SHP100278',
      },
      {
        id: 279,
        name: 'Product 279',
        ref: 'SHP100279',
      },
      {
        id: 280,
        name: 'Product 280',
        ref: 'SHP100280',
      },
      {
        id: 281,
        name: 'Product 281',
        ref: 'SHP100281',
      },
      {
        id: 282,
        name: 'Product 282',
        ref: 'SHP100282',
      },
      {
        id: 283,
        name: 'Product 283',
        ref: 'SHP100283',
      },
      {
        id: 284,
        name: 'Product 284',
        ref: 'SHP100284',
      },
      {
        id: 285,
        name: 'Product 285',
        ref: 'SHP100285',
      },
      {
        id: 286,
        name: 'Product 286',
        ref: 'SHP100286',
      },
      {
        id: 287,
        name: 'Product 287',
        ref: 'SHP100287',
      },
      {
        id: 288,
        name: 'Product 288',
        ref: 'SHP100288',
      },
      {
        id: 289,
        name: 'Product 289',
        ref: 'SHP100289',
      },
      {
        id: 290,
        name: 'Product 290',
        ref: 'SHP100290',
      },
      {
        id: 291,
        name: 'Product 291',
        ref: 'SHP100291',
      },
      {
        id: 292,
        name: 'Product 292',
        ref: 'SHP100292',
      },
      {
        id: 293,
        name: 'Product 293',
        ref: 'SHP100293',
      },
      {
        id: 294,
        name: 'Product 294',
        ref: 'SHP100294',
      },
      {
        id: 295,
        name: 'Product 295',
        ref: 'SHP100295',
      },
      {
        id: 296,
        name: 'Product 296',
        ref: 'SHP100296',
      },
      {
        id: 297,
        name: 'Product 297',
        ref: 'SHP100297',
      },
      {
        id: 298,
        name: 'Product 298',
        ref: 'SHP100298',
      },
      {
        id: 299,
        name: 'Product 299',
        ref: 'SHP100299',
      },
    ],
    selectedProducts: [],
  },
  actions: {
    removeItem(context, id) {
      context.commit('removeItem', id);
    },
  },
  mutations: {
    removeItem(state, id) {
      state.selectedProducts = state.selectedProducts.filter(product => product.id !== id);
    },
  },
  modules: {
  },
});
